const Sequelize = require('sequelize');
const mysql = require('mysql2');

const sequelize = new Sequelize('Sequelize_checker', 'root', 'root', {
  host: 'localhost',
  dialect: 'mysql',
  // dialect: 'mysql'|'sqlite'|'postgres'|'mssql',
  operatorsAliases: false,

  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  },

  // SQLite only
  storage: 'path/to/database.sqlite'
});

module.exports.init = function(callback) {
    // var dbName = 'Sequelize_checker',
    //     username = 'root',
    //     password = 'root',
    //     host = 'localhost';
    //     // var conStringPri = 'postgres://' + username + ':' + password + '@' + host + '/postgres';
    //     // var conStringPost = 'postgres://' + username + ':' + password + '@' + host + '/' + dbName;
    // 
    //     // const sequelizers = new Sequelize('mysql://root:root@localhost/Sequelize_checker');
    //     const conStringPri = 'mysql://' + username + ':' + password + '@' + host + '/mysql';
    //     const conStringPost = 'mysql://' + username + ':' + password + '@' + host + '/' + dbName;
    //     // const sequelizers = 'mysql://root:root@localhost/Sequelize_checker';
    // // connect to postgres db
    // mysql.connect(conStringPri, function(err, client, done) { 
    //     // create the db and ignore any errors, for example if it already exists.
    //     client.query('CREATE DATABASE ' + dbName, function(err) {
    //       console.log(err, "err");
    //         //db should exist now, initialize Sequelize
    //         var sequelize = new Sequelize(conStringPost);
    //         
    //         sequelize
    //           .authenticate()
    //           .then(() => {
    //             console.log('Connection has been established successfully.');
    //           })
    //           .catch(err => {
    //             console.error('Unable to connect to the database:', err);
    //           });
    //           
    //         callback(sequelize);
    //         client.end(); // close the connection
    //     });
    // });
    // 
    var con = mysql.createConnection({
      host: "localhost",
      user: "root",
      password: "root"
    });

    con.connect(function(err) {
      if (err) throw err;
      console.log("Connected!");
      con.query("SHOW DATABASES", function (err, results) {
        let databaseList = [];
        results.forEach(function(db){
          databaseList.push(db.Database);
        });
        var exists = databaseList.indexOf('Sequelize_checker');
        
        if(exists>=0){ 
          con.query("USE Sequelize_checker", function (err, result) {
            if (err) throw err;
            // con.query("mysqldump student_lib > backup-files.sql; ", function (err, result) {
            //   if (err) throw err;
            //   // mysqldump database > backup-file.sql; 
            //   console.log("Database backup created");
            // });
            console.log("Database Using");
          });
        }else{
          con.query("CREATE DATABASE Sequelize_checker", function (err, result) {
            if (err) throw err;
            // mysqldump -u root -p student_lib > backup-file.sql
            console.log("Database created");
          });
          
        }
        });
    });
};


// Or you can simply use a connection uri
// const sequelize = new Sequelize('postgres://user:pass@example.com:5432/dbname');
// const sequelizers = new Sequelize('mysql://user:pass@localhost:3000/dbname');
// const sequelizers = new Sequelize('mysql://root:root@localhost/Sequelize_checker');
// mysql.connect(sequelizers, function(err, client, done) { 
//        // create the db and ignore any errors, for example if it already exists.
//        client.query('CREATE DATABASE ' + dbName, function(err) {
//            //db should exist now, initialize Sequelize
//            var sequelize = new Sequelize(sequelizers);
//            callback(sequelize);
//            client.end(); // close the connection
//        });
//    });
//    

// const sequelize = new Sequelize('mysql://root:root@example.com:5432/Sequelize_checker');