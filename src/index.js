import React from 'react';
import { render } from 'react-dom';
// import App from './containers/App';
import Apps from './containers/Apps';

// Now we can render our application into it
render( <Apps />, document.getElementById('app') );
// render( <App />, document.getElementById('app') );
