// import React from 'react'
// 
// const About = () => (
//   <div>
//     <h1>About Page</h1>
//     <p>Did you get here via Redux?</p>
//   </div>
// )
// 
// export default About
// 

import React, { Component } from 'react';
import {ipcRenderer} from 'electron';
const {
    fetch_student
} = require ('../../../utils/constants');

class About extends Component {
  constructor(props){
    super(props);
    this.checkFun = this.checkFun.bind(this);
  }  
  checkFun() {
    console.log("checkFun",ipcRenderer);
    ipcRenderer.send(fetch_student, 'ping');
  }
  
  render() {
    return (
      <div>
        <button onClick={this.checkFun}> Check </button>
      </div>
    );
  }
}

export default About;

