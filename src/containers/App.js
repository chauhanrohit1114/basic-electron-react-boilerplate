import React, { Component } from 'react';

import '../assets/css/App.css';
import {ipcRenderer} from 'electron';
import HelloWorld from '../components/App/HelloWorld';
const {
    fetch_student
} = require ('../../utils/constants');

class App extends Component {
  constructor(props){
    super(props);
    this.checkFun = this.checkFun.bind(this);
  }  
  checkFun() {
    console.log("checkFun",ipcRenderer);
    ipcRenderer.send(fetch_student, 'ping');
  }
  
  render() {
    return (
      <div>
        <HelloWorld />
        <button onClick={this.checkFun}> Check </button>
      </div>
    );
  }
}

export default App;
