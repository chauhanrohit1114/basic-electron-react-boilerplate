import React from 'react';
import Home from './home';
import About from './about'
import Student from './student';
import { Route, Switch } from 'react-router-dom'

const Routes = (props) => (
  <Switch>
    <Route exact path="/" component={Home} />
    <Route exact path="/about-us" component={About} />
    <Route exact path="/student" component={Student} />
  </Switch>
);
export default (Routes);